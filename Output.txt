Input line: 1-
Create a sequence
-------------------------
Exception - no data was created



Input line: 1 - 100,400,200,700,500
Create a sequence
-------------------------
The sequence: 100 400 200 700 500
Number of elements: 5
The current element: 500


Input line: 2 - 400
Delete a number
-------------------------
The sequence: 100 200 700 500
Number of elements: 4
The current element: 200


Input line: 4 - 3,700
Add a number before anther number
-------------------------
The sequence: 100 200 700 700 500
Number of elements: 5
The current element: 700


Input line: 5 - 5,200
Add a number after a number
-------------------------
The sequence: 100 200 700 700 500 200
Number of elements: 6
The current element: 200


Input line: 3
Delete the first number from the sequence
-------------------------
The sequence: 200 700 700 500 200
Number of elements: 5
The current element: 200


Input line: 6 - 25
Add a number to the end of the sequence
-------------------------
The sequence: 200 700 700 500 200 25
Number of elements: 6
The current element: 25


Input line: 7 - 3
Display a number at a certain index
-------------------------
The sequence: 200 700 700 500 200 25
Number of elements: 6
The current element: 500


Input line: 8
Display the last element in the sequence
-------------------------
The sequence: 200 700 700 500 200 25
Number of elements: 6
The current element: 25


Input line: 1 - 10,20,30
Create a sequence
-------------------------
The sequence: 10 20 30
Number of elements: 3
The current element: 30


Input line: 10
Append another sequence to the first sequence
-------------------------
The sequence: 200 700 700 500 200 25 10 20 30
Number of elements: 9
The current element: 25


Input line: 11
Create a clone sequence
-------------------------
The sequence: 10 20 30
Number of elements: 3
The current element: 30


Input line: 9 - 700,7
Replace a number with another number
-------------------------
The sequence: 200 7 700 500 200 25 10 20 30
Number of elements: 9
The current element: 7


Input line: 12 - 1
Print the sequence
-------------------------
The sequence: 200 7 700 500 200 25 10 20 30
Number of elements: 9
The current element: 30


Input line: 12 - 2
Print the sequence
-------------------------
The sequence: 10 20 30
Number of elements: 3
The current element: 30


