/* Written by Fig and Wyatt J. Brown.
 * Due date: 2015-11-04
 * Description: A program that implements and tests a class that represents an
 * abstract linked list of doubles. */
package edu.monroecc.student.jruiz13ANDwbrown49.lab3;

/** The class {@code SequenceTest} implements methods for testing the
 * correctness of the {@code DoubleLinkedSeq} class.
 *
 * @author Fig
 * @author Wyatt J. Brown
 * @see DoubleLinkedSeq */
public class SequenceTest
{
	// sq1 - the original sequence,
	// sq2 - the second sequence to be created,
	// sq3 - for the cloned sequence.
	private DoubleLinkedSeq sq1, sq2, sq3 = null;

	/** Returns an object of the class DoubleLinkedSeq. The method will parse
	 * the string parameter, create a token for each number in the string, and
	 * will add it to the sequence one number at a time.
	 *
	 * @param st A String containing tokens.
	 * @return An object of the class DoubleLinkedSeq with the parsed tokens as
	 *         elements. */
	public DoubleLinkedSeq createSequence(final String st)
	{
		final String[] commands = st.split("-|,");
		final DoubleLinkedSeq doubleLinkedSeq = new DoubleLinkedSeq();
		for (int iteration = 1; iteration < commands.length; ++iteration)
			doubleLinkedSeq.addEnd(Double.parseDouble(commands[iteration]));
		return doubleLinkedSeq;
	}

	/** Finds element num in the sequence. If the element was found, then the
	 * method returns the index. Else, the method returns -1.
	 *
	 * @param num An int to search for in the sequence.
	 * @param doubleLinkedSeq The DoubleLinkedSeq object to be searched.
	 * @return If the element was found, then the method returns the index.
	 *         Else, the method returns -1. */
	public int find(final double num, final DoubleLinkedSeq doubleLinkedSeq)
	{
		for (int iteration = 0; iteration < doubleLinkedSeq.size(); ++iteration)
			if (num == doubleLinkedSeq.retrieveElement(iteration))
				return iteration;
		return -1;
	}

	/** Creates a menu with the following options:
	 * <ol>
	 * <li>Create a sequence</li>
	 * <li>Delete a number</li>
	 * <li>Delete the first number from the sequence</li>
	 * <li>Add a number before another number</li>
	 * <li>Add a number after a number</li>
	 * <li>Add a number to the end of the sequence</li>
	 * <li>Display a number at a certain index</li>
	 * <li>Display the last element in the sequence</li>
	 * <li>Replace a number with another number</li>
	 * <li>Append another sequence to the first sequence</li>
	 * <li>Create a clone sequence</li>
	 * <li>Print the sequence</li>
	 * <li>Quit</li>
	 * </ol>
	 *
	 * @param st a String parameter that has the option to select from the menu,
	 *        along with the other numbers that are related to that option. */
	public void menu(final String st)
	{
		final String[] commands = st.replaceAll("\\s", "").split("-|,");
		switch (commands[0])
		{
		case "1":// Create a sequence.
			if (commands.length > 1)// If there is data.
				// Determine the current DoubleLinkedSeq.
				if (sq1 == null)
					sq1 = createSequence(st);
				else if (sq2 == null)
					sq2 = createSequence(st);
				else
					sq3 = createSequence(st);
			printSequence(st, null);
			break;
		case "2":// Delete a number.
			final int indexToDelete =
					find(Double.parseDouble(commands[1]), sq1);
			sq1.setCurrent(indexToDelete);
			sq1.removeCurrent();
			printSequence(st, sq1);
			break;
		case "3":// Delete the first number from the sequence.
			sq1.removeFront();
			printSequence(st, sq1);
			break;
		case "4":// Add a number before another number.
			sq1.setCurrent(Integer.parseInt(commands[1]));
			sq1.addBefore(Integer.parseInt(commands[2]));
			printSequence(st, sq1);
			break;
		case "5":// Add a number after a number.
			sq1.setCurrent(Integer.parseInt(commands[1]) - 1);
			sq1.addAfter(Integer.parseInt(commands[2]));
			printSequence(st, sq1);
			break;
		case "6":// Add a number to the end of the sequence.
			sq1.addEnd(Integer.parseInt(commands[1]));
			printSequence(st, sq1);
			break;
		case "7":// Display a number at a certain index.
			sq1.setCurrent(Integer.parseInt(commands[1]));
			printSequence(st, sq1);
			break;
		case "8":// Display the last element in the sequence.
			sq1.currentLast();
			printSequence(st, sq1);
			break;
		case "9":// Replace a number with another number.
			final int indexToReplace =
					find(Double.parseDouble(commands[1]), sq1);
			sq1.setCurrent(indexToReplace);
			sq1.addAfter(Integer.parseInt(commands[2]));
			sq1.setCurrent(indexToReplace);
			sq1.removeCurrent();
			printSequence(st, sq1);
			break;
		case "10":// Append another sequence to the first sequence.
			sq1.addAll(sq2);
			printSequence(st, sq1);
			break;
		case "11":// Create a clone sequence.
			sq3 = (DoubleLinkedSeq) sq2.clone();
			printSequence(st, null);
			break;
		case "12":// Print the sequence.
			switch (Integer.parseInt(commands[1]))
			{
			case 1:
				printSequence(st, sq1);
				break;
			case 2:
				printSequence(st, sq2);
				break;
			case 3:
				printSequence(st, sq3);
				break;
			}
			break;
		case "13":// Quit.
			System.exit(0);// Exit cleanly.
		}
	}

	/** Displays:
	 * <ul>
	 * <li>a comment what is being done; example: insert number 5 after the
	 * number 520</li>
	 * <li>The sequence,</li>
	 * <li>the number of elements in the sequence</li>
	 * <li>the current element.</li>
	 * </ul>
	 *
	 * @param st A String containing tokens.
	 * @param formalSeq The DoubleLinkedSeq object to display. */
	public void printSequence(final String st, final DoubleLinkedSeq formalSeq)
	{
		// Determine the current DoubleLinkedSeq.
		DoubleLinkedSeq doubleLinkedSeq;
		if (formalSeq == null)
		{
			if (sq3 != null)
				doubleLinkedSeq = sq3;
			else if (sq2 != null)
				doubleLinkedSeq = sq2;
			else
				doubleLinkedSeq = sq1;
		} else
			doubleLinkedSeq = formalSeq;

		System.out.println("Input line: " + st);
		final String[] commands = st.split("-|,");
		switch (commands[0].replaceAll("\\s", ""))
		{
		case "1":
			System.out.println("Create a sequence");
			break;
		case "2":
			System.out.println("Delete a number");
			break;
		case "3":
			System.out.println("Delete the first number from the sequence");
			break;
		case "4":
			System.out.println("Add a number before anther number");
			break;
		case "5":
			System.out.println("Add a number after a number");
			break;
		case "6":
			System.out.println("Add a number to the end of the sequence");
			break;
		case "7":
			System.out.println("Display a number at a certain index");
			break;
		case "8":
			System.out.println("Display the last element in the sequence");
			break;
		case "9":
			System.out.println("Replace a number with another number");
			break;
		case "10":
			System.out.println("Append another sequence to the first sequence");
			break;
		case "11":
			System.out.println("Create a clone sequence");
			break;
		case "12":
			System.out.println("Print the sequence");
			break;
		}
		System.out.println("-------------------------");
		if (doubleLinkedSeq != null)// If there is data.
		{
			final int currentElement = (int) doubleLinkedSeq.getCurrent();
			System.out.print("The sequence: ");
			for (int iteration = 0; iteration < doubleLinkedSeq
					.size(); ++iteration)
			{
				System.out.print(
						(int) doubleLinkedSeq.retrieveElement(iteration));
				// Do not print the last space.
				if (iteration != doubleLinkedSeq.size() - 1)
					System.out.print(" ");
			}
			System.out.println();
			System.out.println("Number of elements: " + doubleLinkedSeq.size());
			System.out.println("The current element: " + currentElement);
		} else
			System.out.println("Exception - no data was created\n");
		System.out.println("\n");
	}
}
